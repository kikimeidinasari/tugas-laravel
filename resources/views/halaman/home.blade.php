{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PKS Digital School</title>
</head>
<body> --}}

    @extends('layout.master')
    @section('judul')
        <h1>Media Online</h1>
        <h3>Sosial Media Developer</h3>
    @endsection
        
    @section('content')
        
    
        <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    
        <h3>Benefit Join di Media Online</h3>
        <ul>
            <li>Mendapatkan motivasi dari sesama para Developer</li>
            <li>Sharing knowlenge</li>
            <li>Dibuat oleh calon web Developer terbaik</li>
        </ul>
        <h3>Cara Bergabung ke Media Online</h3>
        <ol>
            <li>Mengunjungi Website ini</li>
            <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
            <li>Selesai</li>
        </ol>
    @endsection

{{-- </body>
</html> --}}