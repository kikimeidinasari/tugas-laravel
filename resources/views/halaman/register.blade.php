<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PKS Digital School</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <form action="/welcome" method="post">
        @csrf
        <label>First name :</label>
            <p><input type="text" name="firstname" placeholder=""/></p>
        <label>Last name :</label>
            <p><input type="text" name="lastname" placeholder=""/></p>
        <label>Gender</label>
            <p>
                <input type="radio" name="gender">Male<br>
                <input type="radio" name="gender">Female<br>
                <input type="radio" name="gender">Other<br>
            </p>
        <label>Nationality</label>
            <p>
                <select name="nationality">
                <option value="indonesia" selected="selected">Indonesia</option>
                <option value="english">English</option>
                <option value="other">Other</option>
                </select>
            </p>
        <label>Language Spoken</label>
             <p>
                <input type="checkbox" name="language">Bahasa Indonesia<br>
                <input type="checkbox" name="language">English<br>
                <input type="checkbox" name="language">Other<br>
            </p>
        <label>Bio</label>
            <p>
                <textarea name="messsage" cols="40" rows="10"></textarea>
            </p>
            <p>
                <input type="submit" value="Kirim">
            </p>
            <style>
                a.button {
                    text-decoration: none;
                    color: initial;
                }
            </style>       

</body>
</html>